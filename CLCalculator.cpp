//
// Created by adrian on 5/22/15.
//

#include <iostream>

#include "debug.h"
#include "error_codes.h"
#include "CLCalculator.h"
#include <sstream>

/*
 * std::to_string is not a member of std bug, we just work around this
 */
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

CLCalculator::CLCalculator(const std::string &kernelSource, const int length, const int prefixLength, int difficulty) {
    this->prefixLength = prefixLength;
    source_str  = new char[kernelSource.length()];
    source_size = length;
    for(int i=0;i<kernelSource.length();++i) {
        source_str[i] = kernelSource[i];
    }
    this->difficulty = difficulty;
    init();
}

CLCalculator::CLCalculator(const unsigned char *kernelSource, const int length, const int prefixLength, int difficulty) {
    this->prefixLength = prefixLength;
    source_str  = new char[length];
    source_size = length;
    for(int i=0;i<length;++i) {
        source_str[i] = kernelSource[i];
    }
    this->difficulty = difficulty;
    init();
}

void CLCalculator::init() {
    device_id = NULL;
    context = NULL;
    command_queue = NULL;
    datastring_mem = NULL;
    hashstring_mem = NULL;
    seed_mem = NULL;
    lock_mem = NULL;
    gpuSeed_mem = NULL;
    program = NULL;
    kernel = NULL;
    platform_id = NULL;

    global_work_size = 1<<(GPU_SEED_LENGTH*4);

    FULL_OUTPUT(std::cout << "Get Platform and Device Info" << std::endl);
    START_OUTPUT get_error_string(clGetPlatformIDs(1, &platform_id, &ret_num_platforms)) END_OUTPUT;
    START_OUTPUT get_error_string(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1,
    &device_id, &ret_num_devices)) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create OpenCL context" << std::endl);
    context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create Command Queue" << std::endl);
    command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create Data Memory Buffer" << std::endl);
    datastring_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, prefixLength * sizeof(char), NULL, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create Seed Memory Buffer" << std::endl);
    seed_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, CPU_SEED_LENGTH * sizeof(char), NULL, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create Seed Memory Buffer" << std::endl);
    lock_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(long), NULL, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create Hash Memory Buffer" << std::endl);
    hashstring_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, 8 * sizeof(int), NULL, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create Hash Memory Buffer" << std::endl);
    gpuSeed_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, (prefixLength + CPU_SEED_LENGTH + GPU_SEED_LENGTH) * sizeof(char), NULL, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    std::string options = "";
    options.append("#define DATASTRING_LENGTH " + patch::to_string(prefixLength) + "\n");
    options.append("#define CPU_SEED_LENGTH " + patch::to_string(CPU_SEED_LENGTH) + "\n");
    options.append("#define GPU_SEED_LENGTH " + patch::to_string(GPU_SEED_LENGTH) + "\n");
    options.append("#define CHECK_ZERO(x) (");

    int i;
    for(i=0; i<(int)(difficulty/32); ++i) {
        options.append("x[" + patch::to_string(i) + "] == 0 && ");
    }

    std::string lastBytes = "";
    for (int i=0; i<32; ++i) {
        if (i<difficulty%32) {
            lastBytes.append("0");
        } else {
            lastBytes.append("1");
        }
    }
    char *ptr;
    options.append("(x[" + patch::to_string(i) + "] | " + patch::to_string(strtol(lastBytes.c_str(), &ptr, 2)) + ") == " + patch::to_string(strtol(lastBytes.c_str(), &ptr, 2)) + ")\n");

    std::string kernelSourceStr = options;
    kernelSourceStr.append(source_str, source_size);
    const char *kernelSource = kernelSourceStr.c_str();
    size_t kernelSourceLength = kernelSourceStr.length();

    FULL_OUTPUT(std::cout << "Create Kernel Program from the source" << std::endl);
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource,
                                        (const size_t *) &kernelSourceLength, &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Build Kernel Program" << std::endl);
    START_OUTPUT get_error_string(clBuildProgram(program, 1, &device_id, NULL, NULL, NULL)) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Create OpenCL Kernel" << std::endl);
    kernel = clCreateKernel(program, "sha", &ret);
    START_OUTPUT get_error_string(ret) END_OUTPUT;

    FULL_OUTPUT(std::cout << "Set OpenCL Kernel Parameters" << std::endl);
    START_OUTPUT get_error_string(clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *) &datastring_mem)) END_OUTPUT;
    START_OUTPUT get_error_string(clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *) &seed_mem)) END_OUTPUT;
    START_OUTPUT get_error_string(clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *) &hashstring_mem)) END_OUTPUT;
    START_OUTPUT get_error_string(clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *) &lock_mem)) END_OUTPUT;
    START_OUTPUT get_error_string(clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *) &gpuSeed_mem)) END_OUTPUT;

    unsigned int lock_v = 0, *lock = &lock_v;
    FULL_OUTPUT(std::cout << "Copy lock buffer to device" << std::endl);
    cl_event evt;
    START_OUTPUT get_error_string(clEnqueueWriteBuffer(command_queue, lock_mem, CL_TRUE, 0,
                                                       sizeof(unsigned int), lock, 0, NULL,
                                                       &evt)) END_OUTPUT;
    START_OUTPUT get_error_string(clWaitForEvents(1, &evt)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseEvent(evt)) END_OUTPUT;


    prefix = new char[prefixLength];
    seed = new char[CPU_SEED_LENGTH];
}

CLCalculator::~CLCalculator() {
    FULL_OUTPUT(std::cout << "Finalization" << std::endl);
    START_OUTPUT get_error_string(clFlush(command_queue)) END_OUTPUT;
    START_OUTPUT get_error_string(clFinish(command_queue)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseKernel(kernel)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseProgram(program)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseMemObject(datastring_mem)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseMemObject(hashstring_mem)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseCommandQueue(command_queue)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseContext(context)) END_OUTPUT;

    free(prefix);
    free(seed);
    free(source_str);
}

void CLCalculator::updatePrefix(char *prefix) {
    for(int i=0; i< prefixLength; ++i) {
        this->prefix[i] = prefix[i];
    }
    copyPrefix();
}

void CLCalculator::updatePrefix(std::string prefix) {
    for(int i=0; i< prefixLength; ++i) {
        this->prefix[i] = prefix[i];
    }
    copyPrefix();
}

void CLCalculator::copyPrefix() {
    FULL_OUTPUT(std::cout << "Copy data buffer to device" << std::endl);
    cl_event evt1;
    START_OUTPUT get_error_string(clEnqueueWriteBuffer(command_queue, datastring_mem, CL_TRUE, 0,
                                                       prefixLength * sizeof(char), prefix, 0, NULL,
                                                       &evt1)) END_OUTPUT;
    START_OUTPUT get_error_string(clWaitForEvents(1, &evt1)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseEvent(evt1)) END_OUTPUT;

    cl_event evt2;
    unsigned int lock_v = 0, *lock = &lock_v;
    FULL_OUTPUT(std::cout << "Copy lock buffer to device" << std::endl);
    START_OUTPUT get_error_string(clEnqueueWriteBuffer(command_queue, lock_mem, CL_TRUE, 0,
                                                       sizeof(unsigned int), lock, 0, NULL,
                                                       &evt2)) END_OUTPUT;
    START_OUTPUT get_error_string(clWaitForEvents(1, &evt2)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseEvent(evt2)) END_OUTPUT;
}

void CLCalculator::updateSeed(char *seed) {
    for(int i=0; i<CPU_SEED_LENGTH; ++i) {
        this->seed[i] = seed[i];
    }
    copySeed();
}

void CLCalculator::updateSeed(std::string seed) {
    for(int i=0; i<CPU_SEED_LENGTH; ++i) {
        this->seed[i] = seed[i];
    }
    copySeed();
}

void CLCalculator::copySeed() {
    FULL_OUTPUT(std::cout << "Copy seed buffer to device" << std::endl);
    cl_event evt;
    START_OUTPUT get_error_string(clEnqueueWriteBuffer(command_queue, seed_mem, CL_TRUE, 0,
                                                       CPU_SEED_LENGTH * sizeof(char), seed, 0, NULL, &evt)) END_OUTPUT;
    START_OUTPUT get_error_string(clWaitForEvents(1, &evt)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseEvent(evt)) END_OUTPUT;
}

bool CLCalculator::calculate(unsigned int *hash, unsigned char *string) {
    FULL_OUTPUT(std::cout << "Execute OpenCL Kernels in parallel" << std::endl);
    cl_event evt1;
    START_OUTPUT get_error_string(
            clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_work_size, NULL, 0, NULL,
                                   &evt1)) END_OUTPUT;
    START_OUTPUT get_error_string(clWaitForEvents(1, &evt1)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseEvent(evt1)) END_OUTPUT;

    unsigned int found_v = 0, *found = &found_v;
    FULL_OUTPUT(std::cout << "Copy lock from the memory buffer" << std::endl);
    cl_event evt2;
    START_OUTPUT get_error_string(clEnqueueReadBuffer(command_queue, lock_mem, CL_TRUE, 0,
                                                      sizeof(unsigned int), found, 0, NULL, &evt2)) END_OUTPUT;
    START_OUTPUT get_error_string(clWaitForEvents(1, &evt2)) END_OUTPUT;
    START_OUTPUT get_error_string(clReleaseEvent(evt2)) END_OUTPUT;

    if(found_v) {
        FULL_OUTPUT(std::cout << "Copy hash from the memory buffer" << std::endl);
        cl_event evt3;
        START_OUTPUT get_error_string(clEnqueueReadBuffer(command_queue, hashstring_mem, CL_TRUE, 0,
                                                          8 * sizeof(int), hash, 0, NULL, &evt3)) END_OUTPUT;
        START_OUTPUT get_error_string(clWaitForEvents(1, &evt3)) END_OUTPUT;
        START_OUTPUT get_error_string(clReleaseEvent(evt3)) END_OUTPUT;

        FULL_OUTPUT(std::cout << "Copy seed from the memory buffer" << std::endl);
        cl_event evt4;
        START_OUTPUT get_error_string(clEnqueueReadBuffer(command_queue, gpuSeed_mem, CL_TRUE, 0,
                                                          getStringLength() * sizeof(char), string, 0, NULL, &evt4)) END_OUTPUT;
        START_OUTPUT get_error_string(clWaitForEvents(1, &evt4)) END_OUTPUT;
        START_OUTPUT get_error_string(clReleaseEvent(evt4)) END_OUTPUT;

        return true;
    }

    return false;
}

int CLCalculator::getStringLength() {
    return prefixLength + CPU_SEED_LENGTH + GPU_SEED_LENGTH;
}
