//
// Created by adrian on 5/22/15.
//

#ifndef HASHGAME_DEBUG_H
#define HASHGAME_DEBUG_H

//#define DEBUG
#ifdef DEBUG
#define START_OUTPUT std::cout <<
#define END_OUTPUT << std::endl
#define FULL_OUTPUT(A) A
#else
#define START_OUTPUT
#define END_OUTPUT
#define FULL_OUTPUT(A)
#endif

#endif //HASHGAME_DEBUG_H
