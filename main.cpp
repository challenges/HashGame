#define VERSION "0.1"

#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <iostream>
#include <getopt.h>

#include "debug.h"
#include "CLCalculator.h"

/**
 * Command line options
 */
const struct option commandlineoptions[] =
        {
                {"parent",   required_argument, 0, 'p'},
                {"nickname", required_argument, 0, 'n'},
                {"amount",   required_argument, 0, 'a'},
                {"difficulty",   required_argument, 0, 'd'},
                {"version",  no_argument,       0, 'v'},
                {"help",     no_argument,       0, 'h'},
                {0, 0,                          0, 0},
        };

#define MAX_SOURCE_SIZE (0x100000)

const char fileName[] = "./sha.cl";
const unsigned char chars[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

typedef struct {
    std::string string;
    std::string hash;
} HashPair;

unsigned char getChar(const unsigned int id, const int i){
    return chars[(id>>(4*i))&(0xf)];
}

HashPair generateHashOrdered(CLCalculator &calculator, std::string parent, std::string name) {
    std::string prefix = parent + " " + name + " ";

    calculator.updatePrefix(prefix);

    unsigned int hash[8];
    HashPair hashPair;
    unsigned char string[calculator.getStringLength() + 1];
    string[calculator.getStringLength()] = 0;
    char seed[CPU_SEED_LENGTH];

    for (unsigned int i = 0; i < (1 << (CPU_SEED_LENGTH * 4));++i) {
        for(int j=0;j<CPU_SEED_LENGTH;++j){
            seed[j] = getChar(i,j);
        }

        calculator.updateSeed(seed);


        if(calculator.calculate(hash, string)) {
            char buff[64];
            char *buff_ptr = buff;
            for (int idx=0; idx < 8; idx++) {
                sprintf(buff_ptr, "%08x", hash[idx]);
                buff_ptr += 8;
            }
            hashPair.hash = buff;

            hashPair.string = (char*)string;

            break;
        }
    }

    return hashPair;
}


void exit_with_help(char* name) {
    std::cout << "usage: " << name <<
    " [-p|--parent] parent [-n|-nickname] nickname [-a|--amount] amount=1 [-d|--difficulty] difficulty=28 [-v|--version] [-h|--help]" <<
    std::endl;
    std::cout << "\t--parent: \tParent hash to use" << std::endl;
    std::cout << "\t--nickname: \tNickname to use" << std::endl;
    std::cout << "\t--amount: \tAmount of chain hashes to generate and output seed on stdout" << std::endl;
    std::cout << "\t--difficulty: \tAmount of leading zeros in generated hash" << std::endl;
    std::cout << "\t--help: \tPrint out this help message" << std::endl;
    std::cout << "\t--version: \tPrint out version" << std::endl;
    exit(1);
}
/**
 * Main application
 */
int main(int argc, char **argv) {
    //Fetch all commandline options
    int iarg = 0;

    std::string parent = "";
    std::string nickname = "";
    int amount = 1;
    int difficulty = 28;

    while (iarg != -1) {
        iarg = getopt_long(argc, argv, "p:n:a:d:vh", commandlineoptions, 0);

        switch (iarg) {
            //Parent hash
            case 'p':
                parent.assign(optarg);
                break;

                //Nickname
            case 'n':
                nickname.assign(optarg);
                break;

                //Amount
            case 'a':
                amount = atoi(std::string(optarg).c_str());
                break;

                //Difficulty
            case 'd':
                difficulty = atoi(std::string(optarg).c_str());
                break;

                //Help message
            case 'h':
                exit_with_help(argv[0]);

                //Version
            case 'v':
                std::cout << "HashGame SHA-256 version " << VERSION << std::endl;
                return 0;

            case '?':
                return 1;
            default:
                break;
        }
    }

    if(parent.length() == 0 || nickname.length() == 0) {
        std::cout << "Please specify parent hash and nickname. Aborted." << std::endl;
        exit_with_help(argv[0]);
    }
    else if(parent.length() != 64) {
        std::cout << "The given parent hash seems to be invalid. It should be 64 characters long. Aborted." << std::endl;
        exit_with_help(argv[0]);
    }
    else if(amount < 1 || difficulty < 1) {
        std::cout << "The given amount of chain hashes or the given difficulty to compute is zero or less. Aborted." << std::endl;
        exit_with_help(argv[0]);
    }

    std::cout << "Using the following Parameters" << std::endl;
    std::cout << "Parent hash:\t " << parent << std::endl;
    std::cout << "Nickname:\t " << nickname << std::endl;
    std::cout << "Difficulty:\t " << difficulty << std::endl;
    std::cout << "Chain hashes:\t " << amount << std::endl;

    /* Load the source code containing the kernel*/
    FILE *fp;
    char *kernel_source_str;
    size_t kernel_source_size;
    fp = fopen(fileName, "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel from file sha.cl.\n");
        exit(1);
    }
    kernel_source_str = (char *) malloc(MAX_SOURCE_SIZE);
    kernel_source_size = fread(kernel_source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    std::string prefix = parent + " " + nickname + " ";
    CLCalculator calculator(kernel_source_str, kernel_source_size, prefix.length(), difficulty);

    for(int i = 0; i < amount; i++) {
        HashPair hashPair = generateHashOrdered(calculator, parent, nickname);
        std::cout << "Result Seed: " << "\"" << hashPair.string << "\"" << std::endl << "Result Hash: \"" << hashPair.hash << "\"" << std::endl;
        std::cout << "Now uploading.....";
        std::string command = "java UploadSingle ";
        command.append(hashPair.string);
        system(command.c_str());
        std::cout << " done!" << std::endl;
        parent = hashPair.hash;
    }

    free(kernel_source_str);

    return 0;
}