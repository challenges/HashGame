//
// Created by adrian on 5/22/15.
//

#ifndef HASHGAME_CLCALCULATOR_H
#define HASHGAME_CLCALCULATOR_H

// number of chars calculated on cpu
// for each combination of the chars the gpu calculates 16^GPU_SEED_LENGTH hashes
#define CPU_SEED_LENGTH 4
#define GPU_SEED_LENGTH 6

#include <string>
#include <CL/cl.h>


class CLCalculator {
public:
    CLCalculator(const std::string &kernelSource, const int length, const int prefixLength, int difficulty);
    CLCalculator(const unsigned char *kernelSource, const int length, const int prefixLength, int difficulty);
    ~CLCalculator();

    void updatePrefix(char *prefix);
    void updatePrefix(std::string prefix);
    void updateSeed(char *seed);
    void updateSeed(std::string seed);
    bool calculate(unsigned int *hash, unsigned char *string);
    int getStringLength();

private:
    void init();
    void copyPrefix();
    void copySeed();

    cl_device_id device_id;
    cl_context context;
    cl_command_queue command_queue;
    cl_mem datastring_mem;
    cl_mem hashstring_mem;
    cl_mem seed_mem;
    cl_mem lock_mem;
    cl_mem gpuSeed_mem;
    cl_program program;
    cl_kernel kernel;
    cl_platform_id platform_id;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret;
    size_t global_work_size;
    char *source_str;
    size_t source_size;
    int prefixLength;
    int difficulty;

    char *prefix;
    char *seed;

};


#endif //HASHGAME_CLCALCULATOR_H
