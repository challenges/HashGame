import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by philipp on 26.05.15.
 */
public class UploadSingle {

    private static final String USER_AGENT = "Mozilla/5.0";

    public static void main(String[] args) {
        boolean correct = false;
        try {
            correct = sendSeed(args[0], args[1], args[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!correct) {
            System.out.println("WARNING: HASH IS INCORRECT");
        }
    }

    public static boolean sendSeed(String parent, String nickname, String seed) throws Exception {
        return sendGet("http://hash.h10a.de/?raw2&Z=" + parent + "&P=" + nickname + "&R=" + seed);
    }

    // HTTP GET request
    public static boolean sendGet(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        //System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //check result
        return !response.toString().contains("Bad hash");
    }
}
