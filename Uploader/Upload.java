import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by philipp on 26.05.15.
 */
public class Upload {
    private static final String USER_AGENT = "Mozilla/5.0";

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("Uploading: " + line);
                String[] input = line.split(" ");
                boolean correct = sendSeed(input[0], input[1], input[2]);

                if(!correct) {
                    System.out.println("WARNING: HASH IS INCORRECT");
                    System.exit(1);
                }
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static boolean sendSeed(String parent, String nickname, String seed) throws Exception {
        return sendGet("http://hash.h10a.de/?raw2&Z=" + parent + "&P=" + nickname + "&R=" + seed);
    }

    // HTTP GET request
    public static boolean sendGet(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //check result
        return !response.toString().contains("Bad hash");
    }
}
